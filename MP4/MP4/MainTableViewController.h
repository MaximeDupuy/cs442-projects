//
//  TableViewController.h
//  MP4
//
//  Created by Maxime Dupuy on 09/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MainTableViewController : PFQueryTableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

- (IBAction)logout:(id)sender;
@end
