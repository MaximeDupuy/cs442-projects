//
//  BoardView.h
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BoardViewDelegate;

@interface BoardView : UIView

@property (readonly) double gridWidth;
@property (readonly) double gridHeight;
@property (readonly) double slotDiameter;
@property (strong) id<BoardViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame slotDiameter:(double)diameter;
@end

@protocol BoardViewDelegate <NSObject>
- (void)boardView:(BoardView *)boardView columnSelected:(int)column;
@end
