//
//  ViewController.m
//  MP4
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "BoardViewController.h"
#import <Parse/Parse.h>

@interface BoardViewController ()
- (void) resetGame;
@end

@implementation BoardViewController {
    BOOL animating;
    NSMutableArray *pieces;
    NSString *_gameID;

}

- (void)awakeFromNib
{
    self.gameModel = [[GameModel alloc] init];
    pieces = [NSMutableArray array];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveBoard:(id)sender {
    NSLog(@"SaveBoard");
    
    PFQuery *query = [PFQuery queryWithClassName:@"Games"];
    
    // Retrieve the object by id
    [query getObjectInBackgroundWithId:_gameID block:^(PFObject *game, NSError *error) {
        
        game[@"board"] = self.gameModel.piecesString;
        NSNumber *turn = [NSNumber numberWithInt:self.gameModel.turn];
        game[@"turn"] = turn;
        [game saveInBackground];
        
    }];
    
    //NSLog(@"boardString %@ ",self.gameModel.piecesString);
    
}

- (void) buildBoard:(NSString*)gameID
{
    _gameID = gameID;
    NSLog(@"gameID: %@",_gameID);

    
    PFQuery *query = [PFQuery queryWithClassName:@"Games"];
    [query getObjectInBackgroundWithId:_gameID block:^(PFObject *game, NSError *error) {
        [self constructBoardView:self.boardView fromString:game[@"board"]];
    }];
}


- (void)resetGame
{
    [self.gameModel resetGame];
    
    for (UIView *piece in pieces) {
        [piece removeFromSuperview];
    }
    
    [pieces removeAllObjects];
}

-(void)constructBoardView:(BoardView *)boardView fromString:(NSString *)buildingInstructions{
    
    for (int col = 0; col<7; col++) {
        for (int row=0; row<6; row++) {
            animating = NO;
            NSLog(@"%@",[buildingInstructions substringWithRange:NSMakeRange(6*col+row, 1)]);
            // We need to build the board here

            
        }
    }
}





- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if (animating)
        return;
    if ([self.gameModel processTurnAtCol:column]) {
        UIView *piece = [[UIView alloc]
                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                  -self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter)];
        
        [pieces addObject:piece];
        
        piece.backgroundColor = [self.gameModel currentColor];
        
        [self.view insertSubview:piece belowSubview:self.boardView];
        
        animating = YES;
        [UIView animateWithDuration:0.5
                         animations:^{
                             piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                        (6-([self.gameModel topEmptyRowInCol:column]-1))*self.boardView.gridHeight);
                             
                         } completion:^(BOOL finished) {
                             animating = NO;
                             if (self.gameModel.gameOver) {
                                 NSLog(@"Player %d won", self.gameModel.winner);
                                 
                                 NSString *result;
                                 if (self.gameModel.winner == 42) {
                                     result = @"Tie Game";
                                 } else {
                                     result = [NSString stringWithFormat:@"Player %d Won!!",self.gameModel.winner];
                                 }
                                 
                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over"
                                                                                 message:result
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil];
                                 [alert show];
                                 [self resetGame];
                             }
                         }];
    }
}

@end
