//
//  OpponentTableViewController.h
//  MP4
//
//  Created by Maxime Dupuy on 09/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface OpponentTableViewController : PFQueryTableViewController
- (IBAction)donePressed:(id)sender;

@end
