//
//  OpponentTableViewController.m
//  MP4
//
//  Created by Maxime Dupuy on 09/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "OpponentTableViewController.h"

@interface OpponentTableViewController (){
    NSString *_selectedOpponent;
}

@end

@implementation OpponentTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Parse Table View

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // The className to query on
        self.parseClassName = @"User";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Name";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
    }
    return self;
}

- (PFQuery *)queryForTable
{
    PFQuery *query = [PFUser query];
    return query;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
    static NSString *simpleTableIdentifier = @"usersCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    // Configure the cell
    
    NSString *username = [object objectForKey:@"username"];
    UILabel *usernameLabel = (UILabel*) [cell viewWithTag:101];
    usernameLabel.text = username;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    NSLog(@"_selected opponent = %@",_selectedOpponent);
    
    if ([_selectedOpponent isEqual: username]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *opponent;
    PFObject *object = [self.objects objectAtIndex:indexPath.row];
    opponent = [object objectForKey:@"username"];
    _selectedOpponent = opponent;
    NSLog(@"opponent selected : %@",opponent);
    [self loadObjects];
}


- (IBAction)donePressed:(id)sender {
    // we create the new game here
    
    PFObject *newGame = [PFObject objectWithClassName:@"Games"];
    newGame[@"playerA"] = [PFUser currentUser].username;
    newGame[@"playerB"] = _selectedOpponent;
    newGame[@"turn"]=@1;
    [newGame saveInBackground];
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
