//
//  main.m
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
