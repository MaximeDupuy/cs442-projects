//
//  ViewController.h
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
#import "GameModel.h"

@interface BoardViewController : UIViewController <BoardViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) GameModel *gameModel;
@property (strong, nonatomic) BoardView *boardView;
@end
