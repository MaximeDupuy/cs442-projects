//
//  ViewController.m
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "BoardViewController.h"


@interface BoardViewController ()
- (void) resetGame;
@end

@implementation BoardViewController {
    BOOL animating;
    NSMutableArray *pieces;
    NSTimer *timeBomb;
}

- (void)awakeFromNib
{
    self.gameModel = [[GameModel alloc] init];
    pieces = [NSMutableArray array];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
    [self timer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resetGame
{
    [self.gameModel resetGame];
    
    for (UIView *piece in pieces) {
        [piece removeFromSuperview];
    }
    
    [pieces removeAllObjects];
}

- (void)timer
{
    timeBomb = [NSTimer scheduledTimerWithTimeInterval:6.0
                                     target:self
                                   selector:@selector(timesUp:)
                                   userInfo:nil
                                    repeats:NO];
    
}

- (void)timesUp:(NSTimer*)timer
{
    NSLog(@"Times up");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Times UP"
                                                    message:[NSString stringWithFormat:@"Times up Player %i a random piece will be placed.",self.gameModel.turn]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [alertView cancelButtonIndex]) {
        NSLog(@"The cancel button was clicked for alertView");
        [self boardView:self.boardView columnSelected:(arc4random_uniform(6)+1)];
    }
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if (animating)
        return;
    if ([self.gameModel processTurnAtCol:column]) {
        
        [timeBomb invalidate];
        [self timer];
        
        UIView *piece = [[UIView alloc]
                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                  -self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter)];
        
        [pieces addObject:piece];
        
        piece.backgroundColor = [self.gameModel currentColor];
        
        [self.view insertSubview:piece belowSubview:self.boardView];
        
        animating = YES;
        [UIView animateWithDuration:0.5
                         animations:^{
                             piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                        (6-([self.gameModel topEmptyRowInCol:column]-1))*self.boardView.gridHeight);
                             
                         } completion:^(BOOL finished) {
                             animating = NO;
                             
                             if (self.gameModel.gameOver) {
                                 NSLog(@"Player %d won", self.gameModel.winner);
                                 
                                 NSString *result;
                                 if (self.gameModel.winner == 42) {
                                     result = @"Tie Game";
                                 } else {
                                     result = [NSString stringWithFormat:@"Player %d Won!!",self.gameModel.winner];
                                 }
                                 
                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over"
                                                                                 message:result
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil];
                                 [alert show];
                                 [self resetGame];
                             }
                         }];
    }
}

@end
