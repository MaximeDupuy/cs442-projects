//
//  GameModel.h
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameModel : NSObject

@property (readonly) int turn;
@property (readonly) UIColor *currentColor;
@property (readonly) BOOL gameOver;
@property (readonly) int winner;

- (void)resetGame;
- (BOOL)processTurnAtCol:(int)col;
- (int)topEmptyRowInCol:(int)col;
- (int)pieceForRow:(int)row col:(int)col;

@end
