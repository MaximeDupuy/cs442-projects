//
//  GameModel.m
//  MP3
//
//  Created by Maxime Dupuy on 05/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "GameModel.h"

@implementation GameModel {
    int _turn;
    int _winner;
    int _pieces[6][7];
    int _numPiecesInCol[7];
}

typedef enum{ All, Top,
    TopRight,
    Right,
    BottomRight,
    Bottom,
    BottomLeft,
    Left,
    TopLeft
} Directions;

- (id)init
{
    if (self = [super init]) {
        // initialization
        _turn = 1;
    }
    return self;
}

- (void)resetGame
{
    _turn = 1;
    _winner = 0;
    for (int i=0; i<7; i++) {
        _numPiecesInCol[i] = 0;
    }
    
    // reset the _pieces Table
    for (int col=0; col<7; col++) {
        for (int row=0; row<6; row++) {
            _pieces[row][col] = 0;
        }
    }
    
}

- (int)turn
{
    return _turn;
}

- (UIColor *)currentColor
{
    if (_turn == 1) {
        return [UIColor redColor];
    } else {
        return [UIColor yellowColor];
    }
}

- (BOOL)gameOver
{
    return _winner != 0;
}

- (int)winner
{
    return _winner;
}

- (BOOL)processTurnAtCol:(int)col
{
    if (_numPiecesInCol[col] == 6) {
        return NO;
    }
    
    // fill the _pieces Table
    
    _pieces[_numPiecesInCol[col]][col-1] = _turn;
    
    _numPiecesInCol[col]++;
    

//    if (col == 3 && _numPiecesInCol[col] == 3) {
//        _winner = _turn;
//    }
    
    // Check of the winner
    [self checkForWinner];
    [self checkForFullBoard];
    
    
    // Change turn
    _turn = (_turn == 1) ? 2 : 1;
    
    return YES;
}

- (void) checkForWinner
{
    //[self printBoard];
    for (int col=0; col<7; col++) {
        for (int row=0; row<6; row++) {
            //Check for all direction
            [self checkForWinAtCol:col andRow:row+1 andCount:0 andDirection:Top];
            [self checkForWinAtCol:col+1 andRow:row+1 andCount:0 andDirection:TopRight];
            [self checkForWinAtCol:col+1 andRow:row andCount:0 andDirection:Right];
            [self checkForWinAtCol:col+1 andRow:row-1 andCount:0 andDirection:BottomRight];
            [self checkForWinAtCol:col andRow:row-1 andCount:0 andDirection:Bottom];
            [self checkForWinAtCol:col-1 andRow:row-1 andCount:0 andDirection:BottomLeft];
            [self checkForWinAtCol:col-1 andRow:row andCount:0 andDirection:Left];
            [self checkForWinAtCol:col-1 andRow:row+1 andCount:0 andDirection:TopLeft];

        }
    }
}

- (void) checkForWinAtCol:(int)col andRow:(int)row andCount:(int)count andDirection:(int)direction
{
    if ([self pieceForRow:row col:col]==_turn) {
        count++;
        //NSLog(@"%i", count);
        if (count==4) {
            _winner = _turn;
        } else {
            switch (direction) {
                case Top:
                    [self checkForWinAtCol:col andRow:row+1 andCount:count andDirection:direction];
                    break;
                case TopRight:
                    [self checkForWinAtCol:col+1 andRow:row+1 andCount:count andDirection:direction];
                    break;
                case Right:
                    [self checkForWinAtCol:col+1 andRow:row andCount:count andDirection:direction];
                    break;
                case BottomRight:
                    [self checkForWinAtCol:col+1 andRow:row-1 andCount:count andDirection:direction];
                    break;
                case Bottom:
                    [self checkForWinAtCol:col andRow:row-1 andCount:count andDirection:direction];
                    break;
                case BottomLeft:
                    [self checkForWinAtCol:col-1 andRow:row-1 andCount:count andDirection:direction];
                    break;
                case Left:
                    [self checkForWinAtCol:col-1 andRow:row andCount:count andDirection:direction];
                    break;
                case TopLeft:
                    [self checkForWinAtCol:col-1 andRow:row+1 andCount:count andDirection:direction];
                    break;
                default:
                    break;
            }
        }
    }
}

- (void) checkForFullBoard
{
    int count = 0;
    for (int col=1; col<8; col++) {
        if (_numPiecesInCol[col]==6) {
            count++;
        }
    }
    //NSLog(@"count %i",count);
    if (count==7) {
        NSLog(@"Tie Game");
        _winner = 42;
    }
}

- (void) printBoard
{
    for (int col=0; col<7; col++) {
        for (int row=0; row<6; row++) {
            NSLog(@"col:%i,row:%i -> %i",col,row,_pieces[row][col]);
        }
    }
    NSLog(@"----------- end ----------- \n");
}

- (int)pieceForRow:(int)row col:(int)col
{
    if (row < 0 || row >6) {
        return 0;
    }
    
    if (col < 0 || col > 7) {
        return 0;
    }
    
    return _pieces[row][col];
}

- (int)topEmptyRowInCol:(int)col
{
    return _numPiecesInCol[col];
}


@end
