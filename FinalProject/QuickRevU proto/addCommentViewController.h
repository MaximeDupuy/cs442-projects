//
//  addCommentViewController.h
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 02/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "revUStarPicker.h"

@interface addCommentViewController : UIViewController <UITextViewDelegate, RateViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *remainLabel;
- (IBAction)donePressed:(id)sender;
- (void) setItemID:(NSString *)given;
@property (weak, nonatomic) IBOutlet revUstarPicker *starPicker;

@property (strong,nonatomic) NSArray *colors;


@end
