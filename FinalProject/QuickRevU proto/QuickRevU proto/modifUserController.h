//
//  modifUserController.h
//  QuickRevU proto
//
//  Created by PIerrick Picon on 03/05/14.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface modifUserController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *pswdChange;

@property (weak, nonatomic) IBOutlet UITextField *confirmPswdChange;

- (IBAction)userNameHasChange:(id)sender;
- (IBAction)emailHasChange:(id)sender;


- (IBAction)changeProfilePict:(id)sender;
- (IBAction)confirmChange:(id)sender;
- (IBAction)fullNameHasChange:(id)sender;

@end
