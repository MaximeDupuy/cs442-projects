//
//  friendPickerTableViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 04/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "friendPickerTableViewController.h"

@interface friendPickerTableViewController (){
    NSMutableArray *_friendList;
}

@end

@implementation friendPickerTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewDidAppear:(BOOL)animated{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Parse Table View

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // The className to query on
        self.parseClassName = @"User";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"name";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
    }
    return self;
}

- (PFQuery *)queryForTable
{
    PFUser* user = [PFUser currentUser];
    _friendList = [NSMutableArray arrayWithArray:[user objectForKey:@"friendsList"]];
    
    //NSLog(@"%@",_friendList);
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" notEqualTo:user.username];
    [query orderByAscending:@"username"];
    return query;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userCell" forIndexPath:indexPath];
    
    UILabel *userNameLabel = (UILabel*) [cell viewWithTag:101];
    UILabel *fullNameLabel = (UILabel*) [cell viewWithTag:102];
    
    userNameLabel.text = [object objectForKey:@"username"];
    fullNameLabel.text = [object objectForKey:@"additional"];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    for (int i=0; i <_friendList.count; i++) {
        if ([_friendList[i]  isEqual:[object objectForKey:@"username"]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *friend;
    PFUser* user = [PFUser currentUser];
    PFObject *object = [self.objects objectAtIndex:indexPath.row];
    friend = [object objectForKey:@"username"];
    
    BOOL alreadyFriend = FALSE;
    
    for (int i=0; i<_friendList.count; i++) {
        if ([_friendList[i]  isEqual: friend]) {
            alreadyFriend = TRUE;
            NSLog(@"%@ already friend",friend);
        }
    }
    
    if (!alreadyFriend) {
        [user addUniqueObject:friend forKey:@"friendsList"];
        NSLog(@"%@ added",friend);

    } else {
        [user removeObject:friend forKey:@"friendsList"];
        NSLog(@"%@ removed",friend);

    }
    [user saveInBackground];
    [self loadObjects];
    
}



@end
