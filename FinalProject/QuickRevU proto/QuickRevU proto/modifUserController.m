//
//  modifUserController.m
//  QuickRevU proto
//
//  Created by PIerrick Picon on 03/05/14.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "modifUserController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"

@interface modifUserController () {
    BOOL imageHasChange;
    BOOL fullNameHasChange;
    BOOL userNameHasChange;
    BOOL emailHasChange;
}

@end

@implementation modifUserController


- (void)viewDidLoad
{
    [super viewDidLoad];
    imageHasChange = false;
    fullNameHasChange = false;
    userNameHasChange = false;
    emailHasChange = false;
    
    PFUser *user = [PFUser currentUser];
    PFFile *userImageFile = user[@"profilePict"];
      [userImageFile getDataInBackgroundWithBlock:^(NSData *profilePict, NSError *error) {
        if (!error) {
               UIImage *image = [UIImage imageWithData:profilePict];
               self.profileImageView.image = image;
           }
       }];

   // PFUser *user = [PFUser currentUser];
    self.userNameTextField.text = [NSString stringWithFormat:@"%@",user.username];
    self.emailTextField.text = [NSString stringWithFormat:@"%@",user.email];
    
    self.fullNameTextField.text = [NSString stringWithFormat:@"%@",user[@"additional"]];


    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
        }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)userNameHasChange:(id)sender {
    userNameHasChange = true;
}

- (IBAction)emailHasChange:(id)sender {
    emailHasChange = true;
}

- (IBAction)changeProfilePict:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    imageHasChange =true;
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)confirmChange:(id)sender {
  
//    NSLog(@"ca yest je suis la ");
    //UIView *superview = textField.superview;
    
    PFUser *user = [PFUser currentUser];
    
    NSString *userNameTextFieldContent = self.userNameTextField.text;
    NSString *fullNameTextFieldContent = self.fullNameTextField.text;
    NSString *emailTextFieldContent = self.emailTextField.text;
    NSString *passwordTextFieldContent = self.pswdChange.text;
    NSString *confPasswordTextFieldContent = self.confirmPswdChange.text;
    UIImage *image = self.profileImageView.image;
    
    
    NSData *profilePict = UIImageJPEGRepresentation(image,0.8);
    
    PFFile *imageFile = [PFFile fileWithName:@"image.jpeg" data: profilePict];
    if (imageHasChange == true){
    user[@"profilePict"]= imageFile;
        NSLog(@"pict Uploaded");
    
    }
    // updated user if changed has been made and if user is not empty

    if (userNameHasChange == true && ![userNameTextFieldContent isEqualToString:@""]){
        user.username = userNameTextFieldContent;
        NSLog(@"user ha been updated");
    }
    //update user if email has been changed and is valid and not empty
    
    if ([self validateEmailWithString: emailTextFieldContent ] && emailHasChange==true)
    {
        user.email = emailTextFieldContent;
        NSLog(@"user email is valid");
    }
    else if (![self validateEmailWithString: emailTextFieldContent ]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The email is not valid"delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    
    if ([passwordTextFieldContent isEqualToString:confPasswordTextFieldContent] && [confPasswordTextFieldContent length] > 0){
        user.password = passwordTextFieldContent;
        //NSLog(@"la taille est de %lu",(unsigned long)[confPasswordTextFieldContent length]);
    }
    else if (![passwordTextFieldContent isEqualToString:confPasswordTextFieldContent]){
             {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"new pswd must be equal to his confirmation"delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        }
    
    if (fullNameHasChange == true && ![fullNameTextFieldContent isEqualToString:@""]){
        user[@"additional"] = fullNameTextFieldContent;
        NSLog(@"fullName Updated");
    }
    
//    NSLog(@"the new name is %@",userNameTextFieldContent);
//    NSLog(@"the new email is %@",emailTextFieldContent);
//    
    
    [user save];

   [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)fullNameHasChange:(id)sender {
    fullNameHasChange = true;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *choosenImage = info[UIImagePickerControllerEditedImage];
    self.profileImageView.image = choosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



@end
