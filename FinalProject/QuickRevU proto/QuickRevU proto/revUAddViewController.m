//
//  revUAddViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 01/04/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "revUAddViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"

@interface revUAddViewController () {
    int _rate;
    NSString *_catSel;
}

@end

@implementation revUAddViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.starPicker.notSelectedImage = [UIImage imageNamed:@"emptyStarfinal.png"];
    self.starPicker.halfSelectedImage = [UIImage imageNamed:@"halfullstarfinal.png"];
    self.starPicker.fullSelectedImage = [UIImage imageNamed:@"fullstarfinal.png"];
    self.starPicker.rating = 0;
    self.starPicker.editable = YES;
    self.starPicker.maxRating = 5;
    self.starPicker.delegate = self;
    
}

- (void) viewDidAppear:(BOOL)animated{
    if (![PFUser currentUser]) { // No user logged in
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)unwind:(UIStoryboardSegue *)unwindSegue
{
    //NSLog(@"View Unwinded");
    self.categoryButton.titleLabel.text = _catSel;
}

- (void)setCategory:(NSString*)category
{
    NSLog(@"category: %@ was received",category);
    _catSel = category;
}



- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    NSLog(@"%f",rating);
    _rate = rating;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender {
    
    PFObject *aRevU = [PFObject objectWithClassName:@"revU"];
    PFUser *user = [PFUser currentUser];
    
    if (self.nameTextField.text.length == 0 || self.commentTextView.text.length == 0 || self.commentTextView.text.length > 140 || _catSel.length == 0 ) {
        
        if (self.nameTextField.text.length == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                            message:@"Please enter a Name."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        } else if (self.commentTextView.text.length == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                            message:@"Please add a comment."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        } else if (_catSel.length == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                            message:@"Please add a category."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        } else if (self.commentTextView.text.length > 140){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                            message:@"Comment too long."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
    }else{
        aRevU[@"Name"]=self.nameTextField.text;
        NSNumber *mark = [NSNumber numberWithInt:_rate];
//        aRevU[@"mark"]= mark;
//        aRevU[@"authorName"]=user.username;
//        aRevU[@"user"]=user;
        aRevU[@"category"]=_catSel;
//        [aRevU addUniqueObjectsFromArray:@[self.conmmentTextField.text] forKey:@"comments"];
        

        
        [aRevU addObject:self.commentTextView.text forKey:@"comments"];
        aRevU[@"numOfComment"] = @1;
        [aRevU addObject:mark forKey:@"markTable"];
        [aRevU addObject:user.username forKey:@"authors"];
        
        
        
        // Picture saving
        NSData *imageData = UIImageJPEGRepresentation(self.imageView.image, 0.8);
        PFFile *imageFile =[PFFile fileWithName:@"picture.jpg" data:imageData];
        [aRevU setObject:imageFile forKey:@"image"];
        
        
        // Show progress
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Uploading";
        [hud show:YES];
        
        
        [aRevU saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [hud hide:YES];
            
            if (!error) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
        //[aRevU saveInBackground];
        

    }
}



#pragma imagePicker Below.

- (IBAction)picChoose:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)textViewDidChange:(UITextView *)textView {
    NSInteger length =  self.commentTextView.text.length;
    self.remainingLabel.text = [NSString stringWithFormat:@"%li",140-length ];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


@end
