//
//  revUDetailViewController.h
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 31/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface revUDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>{
    
    
    
}

- (void)setDetailItem:(NSString*)itemId;

@property (weak, nonatomic) IBOutlet UILabel *markLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UITableView *mytableView;


@end
