//
//  revUMasterViewController.h
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 31/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface revUMasterViewController : PFQueryTableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@end
