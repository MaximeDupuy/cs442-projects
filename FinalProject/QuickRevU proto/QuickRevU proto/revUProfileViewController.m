//
//  revUProfileViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 10/04/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "revUProfileViewController.h"

@interface revUProfileViewController ()

@end

@implementation revUProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    PFUser *user = [PFUser currentUser];
    self.usernameLabel.text = [NSString stringWithFormat:@"Username : %@",user.username];
    self.fullNameLabel.text = [NSString stringWithFormat:@"Full name : %@",user[@"additional"]];
    self.emailLabel.text = [NSString stringWithFormat:@"email : %@",user.email];
    NSDate *createdAt = user.createdAt;
    NSDateFormatter *dateFormatr = [[NSDateFormatter alloc] init];
    [dateFormatr setDateFormat:@"MM/dd/yyyy"];
    self.DateLabel.text = [NSString stringWithFormat:@"User since %@",[dateFormatr stringFromDate:createdAt]];
    
    
    PFFile *userImageFile = user[@"profilePict"];
    [userImageFile getDataInBackgroundWithBlock:^(NSData *profilePict, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:profilePict];
            self.profilePicture.image = image;
            
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logout:(id)sender {
    [PFUser logOut];
    NSLog(@"User log out");
    
    [self.tabBarController setSelectedIndex:0];
}
@end
