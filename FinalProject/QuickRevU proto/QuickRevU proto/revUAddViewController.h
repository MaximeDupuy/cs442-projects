//
//  revUAddViewController.h
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 01/04/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "revUstarPicker.h"
#import "revUMasterViewController.h"

@interface revUAddViewController : UIViewController <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RateViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;

@property (weak, nonatomic) IBOutlet revUstarPicker *starPicker;

@property (weak, nonatomic) IBOutlet UIButton *categoryButton;

- (IBAction)save:(id)sender;

- (IBAction)picChoose:(id)sender;

- (IBAction)unwind:(UIStoryboardSegue *)segue;

- (void)setCategory:(NSString*)category;

@end
