//
//  revUMyLogIn.m
//  QuickRevU proto
//
//  Created by PIerrick Picon on 16/04/14.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "revUMyLogIn.h"
#import <QuartzCore/QuartzCore.h>
@interface revUMyLogIn ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation revUMyLogIn
@synthesize fieldsBackground;
- (void)viewDidLoad
{
    
    
    
    
    
//    [super viewDidLoad];
    
    //[self.logInView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]]];
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo2.png"]]];
    [self.logInView.dismissButton setHidden:true];


    // Add login field background
  //  fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LoginFieldBG.png"]];
    //[self.logInView addSubview:self.fieldsBackground];
    //[self.logInView sendSubviewToBack:self.fieldsBackground];

    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Set frame for elements
    [self.logInView.logo setFrame:CGRectMake(66.5f, 70.0f, 187.0f, 58.5f)];
    [self.fieldsBackground setFrame:CGRectMake(35.0f, 145.0f, 250.0f, 100.0f)];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
