//
//  revUMySignUp.m
//  QuickRevU proto
//
//  Created by PIerrick Picon on 16/04/14.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "revUMySignUp.h"
#import <QuartzCore/QuartzCore.h>

@interface revUMySignUp ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation revUMySignUp
@synthesize fieldsBackground;
- (void)viewDidLoad
{
    //    [super viewDidLoad];
    
    //[self.signUpView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]]];
    [self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo2.png"]]];
    
    
    // Change "Additional" to match our use
    [self.signUpView.additionalField setPlaceholder:@"Full Name"];
    
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Set frame for elements
    [self.signUpView.logo setFrame:CGRectMake(66.5f, 70.0f, 187.0f, 58.5f)];
    [self.fieldsBackground setFrame:CGRectMake(35.0f, 145.0f, 250.0f, 100.0f)];
}


@end
