//
//  main.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 31/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "revUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([revUAppDelegate class]));
    }
}
