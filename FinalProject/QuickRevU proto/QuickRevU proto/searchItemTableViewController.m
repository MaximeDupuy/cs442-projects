//
//  searchItemTableViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 16/04/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "SearchItemTableViewController.h"
#import "revUDetailViewController.h"

@interface searchItemTableViewController () <UISearchDisplayDelegate,UISearchBarDelegate>
{
    NSString *_categoryName;
}

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;

@end

@implementation searchItemTableViewController

- (void)setItemView:(NSString*)categoryID
{
    PFQuery *query = [PFQuery queryWithClassName:@"Category"];
    NSLog(@"Id of category called: %@", categoryID);
    [query getObjectInBackgroundWithId:categoryID block:^(PFObject *item, NSError *error) {
        _categoryName = [item objectForKey:@"name"];
        self.navigationItem.title = _categoryName;
    }];
}



- (void)viewDidLoad
{
    

    self.searchResults = [NSMutableArray array];
    

    
    [super viewDidLoad];
    

}

- (void) viewDidAppear:(BOOL)animated{

    [self loadObjects];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Parse Table View

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // The className to query on
        self.parseClassName = @"revU";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Name";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
    }
    return self;
}

- (PFQuery *)queryForTable
{
    if (_categoryName != nil) {
        PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
        
        if (_categoryName != nil && ![_categoryName isEqual: @"All"]) {
            [query whereKey:@"category" equalTo:_categoryName];
        }
        NSLog(@"query called category is %@",_categoryName);
        return query;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        
        return self.objects.count;
        
    } else {
        
        return self.searchResults.count;
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
    static NSString *simpleTableIdentifier = @"itemCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    if (![tableView isEqual:self.searchDisplayController.searchResultsTableView]) {
        
        // Configure the cell
        
        UILabel *nameLabel = (UILabel*) [cell viewWithTag:101];
        nameLabel.text = [object objectForKey:@"Name"];
        
        UILabel *rattingLabel = (UILabel*) [cell viewWithTag:102];
        
        NSString *avg = [[object objectForKey:@"mark"] stringValue];
        NSRange stringRange = {0, MIN([avg length], 3)};
        avg = [avg substringWithRange:stringRange];
        rattingLabel.text = [NSString stringWithFormat:@"%@/5",avg];
        
        
        // display image
        
        PFFile *thumbnail = [object objectForKey:@"image"];
        PFImageView *thumbnailImageView = (PFImageView*)[cell viewWithTag:100];
        thumbnailImageView.image = [UIImage imageNamed:@"placeholder.png"];
        thumbnailImageView.file = thumbnail;
        [thumbnailImageView loadInBackground];
    }
    
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]) {
        // we didnt manage to reuse the label define earlier !!

        PFObject *resultObject = self.searchResults[indexPath.row];
        cell.textLabel.text = [resultObject objectForKey:@"Name"];
        
        UILabel *nameLabel = (UILabel*) [cell viewWithTag:101];
        nameLabel.text = @"this text dosent appear";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    return cell;
    
}



#pragma mark - Search

- (void)filterResults:(NSString *)searchTerm {
    
    [self.searchResults removeAllObjects];
    
    PFQuery *query = [PFQuery queryWithClassName: self.parseClassName];
    if (![_categoryName  isEqual: @"All"]) {
        [query whereKey:@"category" equalTo:_categoryName];
    }
    [query whereKey:@"Name" containsString:searchTerm];
    
    NSArray *results  = [query findObjects];
    
    NSLog(@"number of result %lu", (unsigned long)results.count);
    
    [self.searchResults addObjectsFromArray:results];
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterResults:searchString];
    return YES;
}

// We had to do that to make the search working

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.reuseIdentifier isEqualToString:@"itemCell"]) {
        
        
        if (self.searchDisplayController.active) {
            NSLog(@"search cell selected");
            [self performSegueWithIdentifier:@"showDetailSearch" sender:cell];
        }
        
        
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"showDetailSearch"]) {
        
        
        if(self.searchDisplayController.active){
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            PFObject *resultObject = self.searchResults[indexPath.row];
            [[segue destinationViewController] setDetailItem:resultObject.objectId];
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            PFObject *object = [self.objects objectAtIndex:indexPath.row];
            [[segue destinationViewController] setDetailItem:object.objectId];
        }
    }
}

@end
