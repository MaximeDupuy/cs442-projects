//
//  revUDetailViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 31/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "revUDetailViewController.h"
#import "addCommentViewController.h"



@interface revUDetailViewController (){
    NSString *_itemID;
    NSMutableArray *_comments;
    NSMutableArray *_markTable;
    NSMutableArray *_authorTable;
}

@end

@implementation revUDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(NSString*)itemID
{

    NSLog(@"%@", itemID);
    _itemID = itemID;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _comments = [[NSMutableArray alloc] init];
	// Do any additional setup after loading the view, typically from a nib
}

- (void) viewDidAppear:(BOOL)animated{
    
    if (_itemID != nil) {
            PFQuery *query = [PFQuery queryWithClassName:@"revU"];
        [query getObjectInBackgroundWithId:_itemID block:^(PFObject *item, NSError *error) {
            // Do something with the returned PFObject
            NSLog(@"%@", item);
            NSString *itemName = [item objectForKey:@"Name"];
            int mark = (int)[[item objectForKey:@"mark"] integerValue];
            self.navigationItem.title = itemName;
            
            NSString *markString = @"";
            for (int i=0; i<5; i++) {
                if (mark!=0) {
                    markString = [markString stringByAppendingString:@"★"];
                    mark--;
                } else {
                    markString =[markString stringByAppendingString:@"☆"];
                }
            }
            self.markLabel.text = markString;
            
            NSDate *createdAt = item.createdAt;
            NSDateFormatter *dateFormatr = [[NSDateFormatter alloc] init];
            [dateFormatr setDateFormat:@"MM/dd/yyyy"];
            
            self.detailLabel.text = [NSString stringWithFormat:@"RevU created the %@.",[dateFormatr stringFromDate:createdAt]];
            
            PFFile *thumbnail = [item objectForKey:@"image"];
            PFImageView *thumbnailImageView = (PFImageView*)[self.view viewWithTag:100];
            thumbnailImageView.image = [UIImage imageNamed:@"placeholder.png"];
            thumbnailImageView.file = thumbnail;
            [thumbnailImageView loadInBackground];
            
            _comments = [NSMutableArray arrayWithArray:[item objectForKey:@"comments"]];
            _markTable = [NSMutableArray arrayWithArray:[item objectForKey:@"markTable"]];
            _authorTable = [NSMutableArray arrayWithArray:[item objectForKey:@"authors"]];
            
            
            for (int i = 0; i<_comments.count; i++) {
                NSLog(@"%i - %@",i,_comments[i]);
            }
            
            [ self.mytableView reloadData];
        }];
    }
    
    if (![PFUser currentUser]) { // No user logged in
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _comments.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comCell" forIndexPath:indexPath];
    UILabel *commentLabel = (UILabel*) [cell viewWithTag:200];
    UILabel *markLabel = (UILabel*) [cell viewWithTag:201];
    UILabel *userLabel = (UILabel*) [cell viewWithTag:202];
    
    commentLabel.text =_comments[indexPath.row];
    userLabel.text = _authorTable[indexPath.row];
    NSNumber *mark = _markTable[indexPath.row];
    markLabel.text= [NSString stringWithFormat:@"%@/5",mark.stringValue];
    
    

    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"newcommentSegue"]) {
        [[segue destinationViewController] setItemID:_itemID];
    }
}


@end
