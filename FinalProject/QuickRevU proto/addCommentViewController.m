//
//  addCommentViewController.m
//  QuickRevU proto
//
//  Created by Maxime Dupuy on 02/05/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "addCommentViewController.h"



@interface addCommentViewController (){
    int _rate;
    NSString* _objectId;
}

@end


@implementation addCommentViewController



- (void) setItemID:(NSString *) given {
    _objectId = given;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.starPicker.notSelectedImage = [UIImage imageNamed:@"emptyStarfinal.png"];
    self.starPicker.halfSelectedImage = [UIImage imageNamed:@"halfullstarfinal.png"];
    self.starPicker.fullSelectedImage = [UIImage imageNamed:@"fullstarfinal.png"];
    self.starPicker.rating = 0;
    self.starPicker.editable = YES;
    self.starPicker.maxRating = 5;
    self.starPicker.delegate = self;
}

- (void) viewDidAppear:(BOOL)animated{
    if (![PFUser currentUser]) { // No user logged in
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
}

- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    NSLog(@"%f",rating);
    _rate = rating;
}

- (void)textViewDidChange:(UITextView *)textView {
    NSInteger length =  self.commentTextView.text.length;
    self.remainLabel.text = [NSString stringWithFormat:@"%li",140-length ];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


- (IBAction)donePressed:(id)sender {
    
    
    
    
    if (self.commentTextView.text.length > 140) {
        NSLog(@"Comment too big");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                        message:@"Your comment is too big..."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else {
        PFQuery *query = [PFQuery queryWithClassName:@"revU"];
        PFUser *user = [PFUser currentUser];
        
        // Retrieve the object by id
        [query getObjectInBackgroundWithId:_objectId block:^(PFObject *item, NSError *error) {
            
            NSNumber *mark = [NSNumber numberWithInt:_rate];
            
            
            
            [item addObject:self.commentTextView.text forKey:@"comments"];
            [item addObject:user.username forKey:@"authors"];
            [item addObject:mark forKey:@"markTable"];
            [item incrementKey:@"numOfComment"];
            [item saveInBackground];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
    }
    
}


@end
