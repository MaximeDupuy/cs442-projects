//
//  MP2DetailTableViewController.m
//  MP2
//
//  Created by Maxime Dupuy on 26/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "MP2DetailTableViewController.h"
#import "MP2Brain.h"

@interface MP2DetailTableViewController () {
    NSArray *_allKey;
    NSString *_category;
    NSMutableArray *_conversions;
    NSDictionary *_abbrevDict;
}

@end

@implementation MP2DetailTableViewController

- (void) setCategoryView: (NSString*) categoryCell{
    self.navigationItem.title = categoryCell;
    _category = categoryCell;
    
    NSDictionary *dict = [ NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"conversions" ofType:@"plist"]];
    NSDictionary *unitDict = [dict objectForKey:categoryCell];
    _abbrevDict = [dict objectForKey:@"Abbreviations"];
    _allKey = [unitDict allKeys];
    // init of the conversions Array with some data.
    _conversions = [MP2Brain convert:[NSMutableArray arrayWithObjects:_category,@"42",[[dict objectForKey:@"Base_Types"]objectForKey:_category], nil]];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _allKey.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell" forIndexPath:indexPath];
    
    UITextField *textField = (UITextField *)[cell.contentView viewWithTag:100];
    textField.text = _conversions[indexPath.row];
    
    // this allow to detect changing in the value of text field
    [textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];

    UILabel *unitLabel = (UILabel *)[cell.contentView viewWithTag:110];
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:111];
    
    
    // Bellow implementation of the abrevation and plurar printing
    
    unitLabel.text = [_abbrevDict objectForKey:_allKey[indexPath.row]];
    
    
    NSInteger conversionValue = [_conversions[indexPath.row] integerValue];
    
    if(conversionValue == 0 || conversionValue == 1){
        // singular names
        nameLabel.text = _allKey[indexPath.row];
    } else {
        // plurar names
        NSString *text = _allKey[indexPath.row];
        if ([text characterAtIndex:(text.length-1)] == 's') {
            //the word already finish with a s
            nameLabel.text = text;
        } else {
            nameLabel.text = [ text stringByAppendingString:@"s"];
        }
    }
    
    return cell;
}

#pragma mark - Text Field Management

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // We create the Done Button
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    id superview = textField.superview;
    while (![superview isKindOfClass:[UITableViewCell class]]) {
        superview = [superview superview];
    }
    NSInteger selectedRow = [self.tableView indexPathForCell:(UITableViewCell *)superview].row;
    
    NSLog(@"Value : %@ for row %li" ,textField.text,selectedRow);
    
    
    NSArray *entryForConvert= [NSArray arrayWithObjects:_category,textField.text,_allKey[selectedRow], nil];
    _conversions = [MP2Brain convert:entryForConvert];
    
    [self.tableView reloadData];
    
}


// This is called when values in the text
-(void)textChanged:(UITextField *)textField
{
      NSLog(@"textfield data %@ ",textField.text);
    
    
    id superview = textField.superview;
    while (![superview isKindOfClass:[UITableViewCell class]]) {
        superview = [superview superview];
    }
    NSInteger selectedRow = [self.tableView indexPathForCell:(UITableViewCell *)superview].row;
    
    NSLog(@"Value : %@ for row %li" ,textField.text,selectedRow);
    
    
    NSArray *entryForConvert= [NSArray arrayWithObjects:_category,textField.text,_allKey[selectedRow], nil];
    _conversions = [MP2Brain convert:entryForConvert];
    
    NSIndexPath* rowToReload;
    NSArray* rowsToReload;
    
    //we reload the data for the cells that aren't edited
    
    for (int i=0; i<_conversions.count; i++)
    {

        if (i!=selectedRow) {
            rowToReload = [NSIndexPath indexPathForRow:i inSection:0];
            //NSLog(@"%i",i);
            rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        }
       
    }
    


    
    //[self.tableView reloadData];
}


- (void) donePressed :(id)sender {
    NSLog(@"Done Pressed");
    
    self.navigationItem.rightBarButtonItem = nil;
    
    [self.view endEditing:YES];

}

@end
