//
//  MP2AppDelegate.h
//  MP2
//
//  Created by Maxime Dupuy on 25/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MP2AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
