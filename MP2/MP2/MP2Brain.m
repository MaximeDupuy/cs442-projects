//
//  MP2Brain.m
//  MP2
//
//  Created by Maxime Dupuy on 29/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "MP2Brain.h"



@implementation MP2Brain


+ (NSMutableArray*) convert:(NSArray *)entry{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // entry[0]:category entry[1]:value entry[2]:unit

    NSString *category = entry[0];
    double value = [entry[1] doubleValue];
    NSString *unit = entry[2];
    
    NSLog(@"Brain : entry sended : %@ %f %@",category,value,unit);
    
    NSDictionary *dict = [ NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"conversions" ofType:@"plist"]];
    NSDictionary *unitDict = [dict objectForKey:category];
    NSString *baseType = [[dict objectForKey:@"Base_Types"] objectForKey:category];
    NSArray *unitDictKeys = unitDict.allKeys;
    
    // step 1 convert to base unit
    double resultOfStep1 = -1;
    int functionType = -1; // 1=constant 2=linear
    
    // what is the function type for this category (constant, linear)
    
    if ([[unitDict objectForKey: unit] isKindOfClass:[NSNumber class] ]) {
        functionType = 1;
    } else if ([[unitDict objectForKey: unit] isKindOfClass:[NSArray class] ]){
        functionType =2;
    }
    
    // we check if the unit is the basetype
    if (![unit isEqualToString: baseType] ) {
        
        NSLog(@"function type %i 1=constant 2=linear",functionType);
        
        // convert to base unit
        
        if (functionType == 1) { //better to use isKindOfClass
            // constant type
            double constant = [[unitDict objectForKey: unit] doubleValue];
            //NSLog(@"The constant is %f",constant);
            resultOfStep1 = [self toBaseValueConstant:value :constant];
        }else if (functionType == 2){
            // linear type

            double coefA = [[unitDict objectForKey:unit][0] doubleValue] ;
            double coefB = [[unitDict objectForKey:unit][1] doubleValue] ;
            //NSLog(@"coeffs a:%f b:%f",coefA,coefB);
            resultOfStep1 = [self toBaseValueLinear:value :coefA :coefB];
        }
                             
    } else {
        // the unit selected was already the base unit
        resultOfStep1 = value;
        //NSLog(@"already the basetype");
    }
    
    
    NSLog(@"resultofStep1 %f",resultOfStep1);
    
    // step 2 do all the other conversion (baseunit -> all)
    
    
    

    NSLog(@"base type %@",baseType);
    
    
    for (int i=0; i<unitDictKeys.count; i++){
        NSLog(@"unitDictKeys %@",unitDictKeys[i]);
        
        if(unitDictKeys[i] == baseType){
            NSLog(@"unitDictKeys %@ %@",unitDictKeys[i],baseType);
            NSString *finalResultString = [NSString stringWithFormat:@"%.3f",resultOfStep1];
            [result insertObject:finalResultString atIndex:i];
            //NSLog(@"result is %@ for %@",finalResultString,unitDictKeys[i]);

        } else {
            if ([[unitDict objectForKey: unitDictKeys[i]] isKindOfClass:[NSNumber class] ]){
                double constant = [[unitDict objectForKey: unitDictKeys[i]] doubleValue];
                //NSLog(@"coef %f for %@",constant,unitDictKeys[i]);
                double finalResult = [self baseToUnitConstant:resultOfStep1 :constant] ;
                //NSLog(@"result is %f for %@",finalResult,unitDictKeys[i]);
                NSString *finalResultString = [NSString stringWithFormat:@"%.3f",finalResult];
                [result insertObject:finalResultString atIndex:i];
            } else if ([[unitDict objectForKey: unitDictKeys[i]] isKindOfClass:[NSArray class] ]){
                double coefA =  [[unitDict objectForKey:unitDictKeys[i]][0] doubleValue];
                double coefB =  [[unitDict objectForKey:unitDictKeys[i]][1] doubleValue];
                NSLog(@"coef %f & %f for %@",coefA,coefB,unitDictKeys[i]);
                double finalResult = [self baseToUnitLinear:resultOfStep1 :coefA :coefB];
                NSString *finalResultString = [NSString stringWithFormat:@"%.3f",finalResult];
                NSLog(@"result is %@ for %@",finalResultString,unitDictKeys[i]);
                [result insertObject:finalResultString atIndex:i];
            }

        }

    }
    
    for (int i=0; i<result.count; i++){
        NSLog(@"result: %@ %@",unitDictKeys[i],result[i]);
    }
    return result;
}


+ (double) toBaseValueConstant: (double)value : (double)constant {
    return value*constant;
}

+ (double) toBaseValueLinear: (double) value : (double) a : (double) b {
    return a*value+b;
}

+ (double) baseToUnitConstant: (double) value : (double) constant {
    return value/constant;
}

+ (double) baseToUnitLinear: (double) value : (double) a : (double) b {
    return (value-b)/a;
}



@end
