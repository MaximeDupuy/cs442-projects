//
//  MP2DetailTableViewController.h
//  MP2
//
//  Created by Maxime Dupuy on 26/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MP2DetailTableViewController : UITableViewController <UITextFieldDelegate>



- (void) setCategoryView: (NSString*) categoryCell;

@end
