//
//  main.m
//  MP2
//
//  Created by Maxime Dupuy on 25/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MP2AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MP2AppDelegate class]));
    }
}
