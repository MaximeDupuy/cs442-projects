//
//  MP2Brain.h
//  MP2
//
//  Created by Maxime Dupuy on 29/03/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MP2Brain : NSObject


+ (NSMutableArray*) convert: (NSArray*)entry;

+ (double) toBaseValueConstant: (double)value : (double)constant;
+ (double) toBaseValueLinear: (double) value : (double) a : (double) b;
+ (double) baseToUnitConstant: (double) value : (double) constant;
+ (double) baseToUnitLinear: (double) value : (double) a : (double) b;


@end
