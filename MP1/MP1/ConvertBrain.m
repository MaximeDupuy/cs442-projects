//
//  ConvertBrain.m
//  MP1
//
//  Created by Maxime Dupuy on 28/02/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "ConvertBrain.h"

@implementation ConvertBrain

+ (double) celsiusToFarenheit:(double)celsiusValue{
    
    double farenheitValue = (celsiusValue*1.8)+32;

    return farenheitValue;
}


+ (double) farenheitToCelsius:(double)farenheitValue{
    
    double celsiusValue = (farenheitValue-32)/1.8;
    
    return celsiusValue;
}

+ (double) kmToMiles:(double)kmValue{
    
    double milesValue = kmValue*0.62137;
    
    return milesValue;
}

+ (double) milesToKm:(double)milesValue{
    
    double kmValue = milesValue/0.62137;
    
    return kmValue;
}

+ (double) litersToGallons:(double)litersValue{
    
    double gallonsValue = litersValue*0.26417;
    
    return gallonsValue;
}

+ (double) gallonsToLiters:(double)gallonsValue{
    
    double littersValue = gallonsValue/0.26417;
    
    return littersValue;
}







@end

