//
//  ConvertBrain.h
//  MP1
//
//  Created by Maxime Dupuy on 28/02/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConvertBrain : NSObject

+ (double) celsiusToFarenheit:(double)celsiusValue;
+ (double) farenheitToCelsius:(double)farenheitValue;
+ (double) kmToMiles:(double)kmValue;
+ (double) milesToKm:(double)milesValue;
+ (double) litersToGallons:(double)litersValue;
+ (double) gallonsToLiters:(double)gallonsValue;

@end




