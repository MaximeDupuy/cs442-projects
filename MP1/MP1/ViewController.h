//
//  ViewController.h
//  MP1
//
//  Created by Maxime Dupuy on 28/02/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConvertBrain.h"

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *topField;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *bottomField;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *convertionDirectionButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *conversionType;

- (IBAction)catetegoryChanged:(id)sender;
- (IBAction)convertButtonTapped:(id)sender;
- (IBAction)convertionDirection:(id)sender;

- (IBAction)topFieldEdited:(id)sender;
- (IBAction)bottomFieldEdited:(id)sender;

- (IBAction)topFieldTouched:(id)sender;
- (IBAction)bottomFieldTouched:(id)sender;

- (void)convert;


@end

bool arrowDown = YES;
