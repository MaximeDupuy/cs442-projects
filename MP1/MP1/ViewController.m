//
//  ViewController.m
//  MP1
//
//  Created by Maxime Dupuy on 28/02/2014.
//  Copyright (c) 2014 Maxime Dupuy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)catetegoryChanged:(id)sender {
    NSLog(@"categoryChanged");
    
    switch (self.conversionType.selectedSegmentIndex) {
        case 0:
            NSLog(@"Temperature");
            self.topLabel.text = @"℉";
            self.bottomLabel.text = @"℃";
            [self convert];
            break;
         case 1:
            NSLog(@"Distance");
            self.topLabel.text = NSLocalizedString(@"MILES", nil);
            self.bottomLabel.text = NSLocalizedString(@"KILOMETERS", nil);
            [self convert];
            break;
        case 2:
            NSLog(@"Volume");
            self.topLabel.text = NSLocalizedString(@"GALLONS",nil);
            self.bottomLabel.text = NSLocalizedString(@"LITERS",nil);
            [self convert];
            break;
        default:
            break;
    }
}
    

- (IBAction)convertButtonTapped:(id)sender {
    
    NSLog(@"Convert Button Tapped");
    
    [self convert];
}

- (IBAction)convertionDirection:(id)sender {
    
    if(arrowDown) {
       [sender setTitle:@"⬆︎" forState:UIControlStateNormal];
        arrowDown = NO;
    } else {
        [sender setTitle:@"⬇︎" forState:UIControlStateNormal];
        arrowDown = YES;
    }
    NSLog(@"convertionDirection is down %d",arrowDown);
    
}

// Insta Conversion
- (IBAction)topFieldEdited:(id)sender {
    [self convert];
}

- (IBAction)bottomFieldEdited:(id)sender {
    [self convert];
}

// Change the conversion direction when fields are touched
- (IBAction)topFieldTouched:(id)sender {
    [self.convertionDirectionButton setTitle:@"⬇︎" forState:UIControlStateNormal];
    arrowDown = YES;
    
    NSLog(@"convertionDirection is down %d",arrowDown);
}

- (IBAction)bottomFieldTouched:(id)sender {

    [self.convertionDirectionButton setTitle:@"⬆︎" forState:UIControlStateNormal];
    arrowDown = NO;
    
    NSLog(@"convertionDirection is down %d",arrowDown);
}

- (void)convert{
    
    switch (self.conversionType.selectedSegmentIndex) {
        case 0:
            // Temperature
            if (arrowDown) {
                // farenheit to celsius
                double value = [self.topField.text doubleValue];
                self.bottomField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain farenheitToCelsius:value]];
            } else {
                // celsius to farenheit
                double value = [self.bottomField.text doubleValue];
                self.topField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain celsiusToFarenheit:value]];
            }
            break;
            
        case 1:
            // Distance
            if (arrowDown) {
                // Miles to km
                double value = [self.topField.text doubleValue];
                self.bottomField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain milesToKm:value]];
            } else {
                // Km to miles
                double value = [self.bottomField.text doubleValue];
                self.topField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain kmToMiles:value]];
            }
            break;
            
        case 2:
            // Volume
            if (arrowDown) {
                // gallons to liters
                double value = [self.topField.text doubleValue];
                self.bottomField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain gallonsToLiters:value]];
            } else {
                // liters to gallons
                double value = [self.bottomField.text doubleValue];
                self.topField.text = [NSString stringWithFormat:@"%.2f", [ConvertBrain litersToGallons:value]];
            }
            break;
            
        default:
            break;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


// Code to dismiss the keyboard when user touch outside textFields
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
